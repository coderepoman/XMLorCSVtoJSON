﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using System.Xml.Linq;
using Formatting = Newtonsoft.Json.Formatting;

namespace LesXMLCSVTilJSON
{
    public static class Program
    {
        private static readonly string filePath = @"..\..\eksempelfiler\test.csv";

        public static void Main(string[] args)
        {
            string jsonOutput;

            if (Path.GetExtension(filePath) == ".xml")
            {
                jsonOutput = readXML();
            } else if (Path.GetExtension(filePath) == ".csv")
            {
                jsonOutput = readCSV();
            } else
            {
                jsonOutput = "Denne applikasjonen kan bare lese .csv og .xml filer.";
            }

            Console.WriteLine(jsonOutput);
        }

        public static string readXML()
        {
            var xmlDoc = XDocument.Load(filePath);

            var jsonDoc = JsonConvert.SerializeXNode(xmlDoc, Formatting.Indented);

            return jsonDoc;
        }

        public static string readCSV()
        {
            var csv = new List<string[]>();
            var lines = File.ReadAllLines(filePath);

            foreach (string line in lines)
                csv.Add(line.Split(','));

            var columns = lines[0].Split(',');

            var listObjResult = new List<Dictionary<string, string>>();

            for (int i = 1; i < lines.Length; i++)
            {
                var objResult = new Dictionary<string, string>();
                for (int j = 0; j < columns.Length; j++)
                    objResult.Add(columns[j], csv[i][j]);

                listObjResult.Add(objResult);
            }

            return JsonConvert.SerializeObject(listObjResult, Formatting.Indented);
        }
    }
}
